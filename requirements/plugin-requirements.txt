# The dependencies listed here are necessary for specifc plugins, but
# aren't required for bst-plugins-experimental to be installed.

# deb source
arpy

# Bazel source
requests

# git_repo source
dulwich
